const path = require('path')

console.log('Склеить путь универсальный для всех операционных систем',path.join(__dirname,'first', 'second', 'third'));
console.log('навигация по папкам',path.join(__dirname,'..', '..', 'test' ));
console.log('Абсолютный путь',path.resolve('/last', 'first', 'test' ));
console.log('Абсолютный путь',path.resolve('last', '/first', 'test' ));
console.log('Абсолютный путь',path.resolve('last', 'first', 'test' ));

const fullpath = path.resolve(__dirname,'first', 'second', 'third.js' );
console.log('парсинг строки' , path.parse(fullpath))

console.log("Разделитель в ОС", path.sep)
console.log("Проверка на абсолютный путь", path.isAbsolute('first/second'))
console.log("Название файла", path.basename(fullpath))
console.log("Расширение файла", path.extname(fullpath))


// ----------------------

const siteURL = 'http://localhost:8080/user?id=5123';

const url = new URL(siteURL);
console.log(url)