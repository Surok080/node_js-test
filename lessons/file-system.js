const fs = require('fs')
const path = require('path')

// console.log('start')
// fs.mkdir(path.resolve(__dirname, 'dir'), (err) => {
//     if (err) {
//         console.log(err, 'err')
//         return;
//     } else {
//         console.log('Папка создана')
//     }
// })
//
// console.log('end')

// fs.rmdir(path.resolve(__dirname, 'dir'), (err) => {
//     if (err) {
//         console.log(err);
//     }
// })

// перезатирает файл-----
// fs.writeFile(path.resolve(__dirname, 'test.txt'), 'fsdlkfjdskjf slakjfkl', (err) => {
//     if (err) {
//         console.log(err);
//     }
// })

// fs.appendFile(path.resolve(__dirname, 'test.txt'), ' Добавленный текст в конец файла', (err) => {
//     if (err) {
//         console.log(err);
//     }
// })



// fs.writeFile(path.resolve(__dirname, 'test.txt'), 'Создали файл', (err) => {
//     if (err) {
//         console.log(err);
//     } else {
//         fs.appendFile(path.resolve(__dirname, 'test.txt'), ' - Добавили текст в конец созданного файла', (err) => {
//             if (err) {
//                 console.log(err);
//             }
//         })
//     }
//
// })

//--------------------------------------
const writeFileAsync = async (path, data) => {
    return new Promise((resolve, reject) => {
        return fs.writeFile(path, data,  (err) => {
            if (err) {
                return reject(err.message)
            }
            resolve()
        })
    })
}

const appendFileAsync = async (path, data) => {
    return new Promise((resolve, reject) => {
        return fs.appendFile(path, data , (err) => {
            if (err) {
                return reject(err.message)
            }
            resolve()
        })
    })
}

// writeFileAsync(path.resolve(__dirname, 'test.txt'), 'Запись в файл в спомощью промиса')
//     .then(() => appendFileAsync(path.resolve(__dirname, 'test.txt'), '\nДозапись после выполнения промиса 1'))
//     .then(() => appendFileAsync(path.resolve(__dirname, 'test.txt'), '\nДозапись после выполнения промиса 2'))
//     .then(() => appendFileAsync(path.resolve(__dirname, 'test.txt'), '\nДозапись после выполнения промиса 3'))
//     .catch((err) => console.log(err))
//--------------------------------------



//--------------------------------------
// const writeFileAsync = async (path, data) => {
//     return new Promise((resolve, reject) => {
//         return fs.writeFile(path, data,  (err) => {
//             if (err) {
//                 return reject(err.message)
//             }
//             resolve()
//         })
//     })
// }

// const appendFileAsync = async (path, data) => {
//     return new Promise((resolve, reject) => {
//         return fs.appendFile(path, data , (err) => {
//             if (err) {
//                 return reject(err.message)
//             }
//             resolve()
//         })
//     })
// }
const readFileAsync = async (path) => {
    return new Promise((resolve, reject) => {
        return fs.readFile(path, {encoding: 'utf-8'}, (err, data) => {
            if (err) {
                return reject(err.message)
            }
            resolve(data)
        })
    })
}

// writeFileAsync(path.resolve(__dirname, 'test.txt'), 'Запись в файл в спомощью промиса')
//     .then(() => appendFileAsync(path.resolve(__dirname, 'test.txt'), '\nДозапись после выполнения промиса 1'))
//     .then(() => appendFileAsync(path.resolve(__dirname, 'test.txt'), '\nДозапись после выполнения промиса 2'))
//     .then(() => appendFileAsync(path.resolve(__dirname, 'test.txt'), '\nДозапись после выполнения промиса 3'))
//     .then(() => readFileAsync(path.resolve(__dirname, 'test.txt')))
//     .then(data => console.log(data))
//     .catch((err) => console.log(err))

//--------------------------------------

const removeFileAsync = async (path) => {
    return new Promise((resolve, reject) => {
        return fs.rm(path,  (err) => {
            if (err) {
                return reject(err.message)
            }
            resolve()
        })
    })
}
//
// removeFileAsync(path.resolve(__dirname, 'test.txt'))
// .then(() => console.log('file was removed 2'))
//--------------------------------------


const text = process.env.TEXT || '';

writeFileAsync(path.resolve(__dirname, 'test.txt'), text)
.then(() => readFileAsync(path.resolve(__dirname, 'test.txt')))
.then(data => data.split(' ').length)
.then(count => writeFileAsync(path.resolve(__dirname, 'count.txt'), `Кол-во слов - ${count}`))
.then(() => removeFileAsync(path.resolve(__dirname, 'test.txt')))